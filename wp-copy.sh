#! /bin/bash
set -eo pipefail

# Steps to migrate a website to SiteGround
# -----
# 1. Log into SiteGround and create a new website. When asked, install WordPress.
# 2. Install your public key on the new site using SiteGround Tools
# 3. Modif the .env file. Set ALL variables. The DEST info you can get from the
#    SiteGround SiteTools SSH screen.
# 4. Run this script

#
# All the other changable environment variables are in the .env file. You
# should not need to change anything beyond this point.
#
source .env

TMP_DIR=/tmp/holding

#
# Functions needed to color the text
#
green_text () {
  tput setaf 2; echo $1;tput sgr0
}
yellow_text () {
  tput setaf 3; echo $1;tput sgr0
}
red_text () {
  tput setaf 1; echo $1;tput sgr0
}
white_text () {
  tput setaf 7; echo $1;tput sgr0
}

echo
green_text "WP Copy"
yellow_text "======="
green_text "(c) 2022  Cal Evans <cal@calevans.com>"
green_text "Copies a WordPress site to a new host."
echo
red_text "WARNING:"
red_text "This process will destroy whatever is currently hosted at the "
red_text "destination. You have been warned."
echo
echo


#
#  Setup needed variables
#
yellow_text "Setting up variables"
ORIGIN_SSH_COMMAND="-l $ORIGIN_USERNAME -p $ORIGIN_PORT -i $ORIGIN_PRIVATE_KEY $ORIGIN_HOST"
ORIGIN_SCP_COMMAND="-P $ORIGIN_PORT $ORIGIN_USERNAME@$ORIGIN_HOST"
DEST_SSH_COMMAND="-l $DEST_USERNAME -p $DEST_PORT -i $DEST_PRIVATE_KEY $DEST_HOST"
DEST_SCP_COMMAND="-P $DEST_PORT $DEST_USERNAME@$DEST_HOST"

ORIGN_BASE_DIR=$(dirname $ORIGIN_PATH);
ORIGN_WEBROOT=$(basename $ORIGIN_PATH);
DEST_BASE_DIR=$(dirname $DEST_PATH);
DEST_WEBROOT=$(basename $DEST_PATH);

ORIGIN_WP_SSH="--ssh=$ORIGIN_USERNAME@$ORIGIN_HOST:$ORIGIN_PORT$ORIGIN_PATH"
DEST_WP_SSH="--ssh=$DEST_USERNAME@$DEST_HOST:$DEST_PORT$DEST_PATH"


#
# Make sure we have access to all needed servers and systems
#
yellow_text "Testing for a sane environment"

if [ -z "$(rsync --version | head -n 1)" ]
then
  red_text "ERROR!"
  red_text "There is no rsync installed locally."
  exit 1
fi

if [ -z "$(ssh $DEST_SSH_COMMAND rsync --version | head -n 1)" ]
then
  red_text "ERROR!"
  red_text "There is no rsync installed locally."
  exit 2
fi


if [ -z "$(wp cli version --quiet)" ]
then
  red_text "ERROR!"
  red_text "There is no wp-cli installed locally."
  exit 3
fi

if [ -z "$(ssh $ORIGIN_SSH_COMMAND "pwd")" ]
then
  red_text "ERROR!"
  red_text "There is no ssh access to the origin host"
  exit 4
fi

if [ -z "$(ssh $DEST_SSH_COMMAND "pwd")" ]
then
  red_text "ERROR!"
  red_text "There is no ssh access to the destination host"
  exit 5
fi

#
# Finish created our needed variables
#
# ORIGIN_WP_SSH="--ssh=$ORIGIN_USERNAME@$ORIGIN_HOST:$ORIGIN_PORT$ORIGIN_PATH"
# DEST_WP_SSH="--ssh=$DEST_USERNAME@$DEST_HOST:$DEST_PORT$DEST_PATH"


if [ -z "$(wp cli version --quiet $ORIGIN_WP_SSH)" ]
then
  red_text "ERROR!"
  red_text "There is no wp-cli on the origin host"
  exit 6
fi

if [ -z "$(wp cli version --quiet $DEST_WP_SSH)" ]
then
  red_text "ERROR!"
  red_text "There is no wp-cli on the destination host"
  exit 7
fi

green_text "We are good to go."

yellow_text "Gathering Origin Information"
ORIGIN_TABLE_PREFIX=$(wp config get table_prefix $ORIGIN_WP_SSH | perl -ne 'chomp and print')
ORIGIN_DB_NAME=$(wp config get DB_NAME $ORIGIN_WP_SSH | perl -ne 'chomp and print')
ORIGIN_DB_USER=$(wp config get DB_USER $ORIGIN_WP_SSH | perl -ne 'chomp and print')
ORIGIN_DB_PASSWORD=$(wp config get DB_PASSWORD $ORIGIN_WP_SSH | perl -ne 'chomp and print')
ORIGIN_DB_HOST=$(wp config get DB_HOST $ORIGIN_WP_SSH | perl -ne 'chomp and print')


yellow_text "Gathering Destination Information"
DEST_DB_NAME=$(wp config get DB_NAME $DEST_WP_SSH | perl -ne 'chomp and print')
DEST_DB_USER=$(wp config get DB_USER $DEST_WP_SSH | perl -ne 'chomp and print')
DEST_DB_PASSWORD=$(wp config get DB_PASSWORD $DEST_WP_SSH | perl -ne 'chomp and print')
DEST_DB_HOST=$(wp config get DB_HOST $DEST_WP_SSH | perl -ne 'chomp and print')


# DB
yellow_text "Transfering the database"
wp db reset --yes --quiet $DEST_WP_SSH
ssh $ORIGIN_SSH_COMMAND \
  mysqldump -u $ORIGIN_DB_USER -p$ORIGIN_DB_PASSWORD --add-drop-table --add-locks --create-options --quick --skip-lock-tables --extended-insert --set-charset --disable-keys $ORIGIN_DB_NAME \
  | wp db import --quiet $DEST_WP_SSH -

# rsync the files downlocal then up to the new site. Then delete the local copy.
yellow_text "Copying files down locally"

rsync  -rz -e "ssh -p $ORIGIN_PORT" $ORIGIN_USERNAME@$ORIGIN_HOST:$ORIGIN_PATH $TMP_DIR
mv $TMP_DIR/$ORIGN_WEBROOT $TMP_DIR/$DEST_WEBROOT

yellow_text "Copying files up to server"
rsync  -rz  $TMP_DIR/$DEST_WEBROOT $DEST_HOST:$DEST_BASE_DIR

yellow_text "Cleaning up local"
rm -rf $TMP_DIR

yellow_text "Repair Permissions"
DEST_OWNER=$(ssh $DEST_SSH_COMMAND "stat -c '%U:%G' $DEST_PATH")
ssh $DEST_SSH_COMMAND "sudo chown -R $DEST_OWNER $DEST_PATH"
ssh $DEST_SSH_COMMAND "sudo chmod -R 770 $DEST_PATH"

yellow_text "Updating wp-config.php settings"
wp config set DB_NAME $DEST_DB_NAME --quiet $DEST_WP_SSH
wp config set DB_USER $DEST_DB_USER --quiet $DEST_WP_SSH
wp config set DB_PASSWORD $DEST_DB_PASSWORD --quiet $DEST_WP_SSH
wp config set DB_HOST $DEST_DB_HOST --quiet $DEST_WP_SSH

#if [ $ORIGIN_IS_MULTISITE -gt 0 ]
#then
#  wp config set DOMAIN_CURRENT_SITE --quiet $DEST_DOMAIN $DEST_WP_SSH
#fi

yellow_text "Search/Replace"
wp search-replace  --quiet $ORIGIN_DOMAIN $DEST_DOMAIN $DEST_MULTISITE_EXTRA $DEST_WP_SSH

echo
green_text "Done"
echo

