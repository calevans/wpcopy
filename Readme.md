# wpcopy.sh

Copyright 2022 E.I.C.C., Inc. All Rights Reserved
Released under the MIT license.

This script copies a WordPress site from one server to another. It was originally
designed to move large sites to SiteGround.com but should be useable in moving
a WordPress site from one site to another.

This project requires `wp-cli` to be installed in 3 places.

- The machine you are running this script on
- The Origin server
- The Destination server

This script is designed to be run locally and not on one of the servers.
The reason for this requirement is because this is how I usually run it.

# Steps to use to move a site to SiteGround.com

1. Create the site on SiteGround
1. Import your public key into the site using SiteGround's SiteTools
1. Modify the `.env` file. Set ALL variables. The DEST info you can get from the SiteGround SiteTools SSH screen.
1. Run the script
1. Change your DNS. (This is outside of the scope of the script)
1. Wait for DNS to propogate
1. Test your site to make sure everything is working properly.